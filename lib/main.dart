import 'dart:math';
import 'package:flutter/material.dart';

void main() => runApp(FortuneTellerApp());

class FortuneTellerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff463E35),
          title: Text(
            "Ask Me Anything",
            style: TextStyle(fontFamily: 'Berkshire Swash'),
          ),
        ),
        backgroundColor: Color(0xff322B26),
        body: FortuneTeller(),
      ),
    );
  }
}

class FortuneTeller extends StatefulWidget {
  @override
  _FortuneTellerState createState() => _FortuneTellerState();
}

class _FortuneTellerState extends State<FortuneTeller> {
  int tellerNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlatButton(
        child: Image.asset('images/fortuneTeller$tellerNumber.png'),
        onPressed: () {
          setState(() {
            tellerNumber = Random().nextInt(3) + 1;
          });
        },
      ),
    );
  }
}
