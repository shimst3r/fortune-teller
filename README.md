# fortune_teller

Fortune Teller is a magic-8-ball-like app that answers all of your yes/no questions to its best knowledge.

![A screenshot taken from the app. It shows a veiled person in front of a crystal ball with the text 'maybe' on it.](screenshot.png)

## Background

This is my third [Flutter](https://flutter.dev) project, implemented as one of the challenges during [The Complete 2020 Flutter Development Bootcamp with Dart](https://www.udemy.com/course/flutter-bootcamp-with-dart/).

## Credits

<a href="https://www.vecteezy.com/free-vector/fortune">Fortune Vectors by Vecteezy</a>

